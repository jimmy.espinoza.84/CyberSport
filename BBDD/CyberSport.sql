USE [CyberSport]
GO
/****** Object:  Table [dbo].[Venta]    Script Date: 07/13/2017 19:48:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Venta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NULL,
	[cliente] [int] NULL,
	[total] [int] NULL,
	[direccion] [int] NULL,
	[nombrerecibe] [varchar](100) NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_Venta] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 07/13/2017 19:48:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](20) NULL,
	[password] [varchar](20) NULL,
	[nombre] [varchar](100) NULL,
	[rut] [varchar](12) NULL,
	[telefono] [varchar](12) NULL,
	[movil] [varchar](12) NULL,
	[email] [varchar](200) NULL,
	[perfil] [int] NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON
INSERT [dbo].[Usuario] ([id], [username], [password], [nombre], [rut], [telefono], [movil], [email], [perfil], [estado]) VALUES (1, N'admin', N'admin', N'Admin CyberSport', N'-', N'-', N'-', N'admin@admin.cl', 1, 1)
INSERT [dbo].[Usuario] ([id], [username], [password], [nombre], [rut], [telefono], [movil], [email], [perfil], [estado]) VALUES (2, NULL, N'jimmy', N'Jimmy Espinoza', N'186767314', N'953723317', NULL, N'jimmy.espinoza.84@gmail.com', NULL, 1)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
/****** Object:  Table [dbo].[Producto]    Script Date: 07/13/2017 19:48:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Producto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [varchar](50) NULL,
	[nombre] [varchar](50) NULL,
	[descripcion] [varchar](100) NULL,
	[categoria] [int] NULL,
	[stock] [int] NULL,
	[precio] [int] NULL,
	[fechacreacion] [datetime] NULL,
	[usuariocreacion] [int] NULL,
	[fechabaja] [datetime] NULL,
	[usuariobaja] [int] NULL,
	[promocion] [int] NULL,
	[preciopromocion] [int] NULL,
	[descuento] [int] NULL,
	[porcentajedescuento] [int] NULL,
	[imagen1] [varchar](100) NULL,
	[imagen2] [varchar](100) NULL,
	[imagen3] [varchar](100) NULL,
	[imagen4] [varchar](100) NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Producto] ON
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (13, N'SKU 4819-279', N'Zapatilla Columbia Redmond', N'Zapatilla Columbia Redmond Mid Waterproof Outdoor ', 1, 10, 56990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'851098-010.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (14, N'SKU 3050-3040', N'Zapatilla Skechers', N'Zapatilla Skechers Equalizer Game Point Urbana Niño', 1, 10, 20990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'114689-007.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (15, N'SKU 1954-3595', N'Zapatilla Columbia', N'Zapatilla Columbia Bugaboot II', 1, 10, 90990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'134094-010.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (16, N'SKU 824-1629', N'Poleron Yamaha', N'Polerón Vertical Zip Azul Yamaha', 2, 10, 25990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'poleronYamahaAzul.png', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (17, N'SKU 5583-687', N'Camiseta Selección Chilena ', N'Camiseta Selección Chilena Nike Hombre Visita', 2, 10, 46990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'camisetachileblanca.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (18, N'SKU 5387-4907', N'Pantalón de Buzo', N'Pantalón de Buzo Jogger OshKosh B’gosh', 2, 10, 16990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'pantalonbuzogris.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (19, N'SKU 1217-3203', N'Balón Basketball', N'Balón Oficial NBA Spalding', 3, 10, 59990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'122879-999.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (20, N'SKU 2757-2056', N'Balón Fútbol ', N'Balón Fútbol Impel N°5 Mitre', 3, 10, 9990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'854480-002.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (21, N'SKU 638-2922', N'Ping Pong', N'Set Ping Pong Nautika', 3, 10, 12990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'819773-999.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (22, N'SKU 5556-574', N'Bolso Adidas', N'Bolso Adidas Deportivo Lin', 4, 10, 13790, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'109243-002.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (23, N'SKU 5797-862', N'Jockey Puma', N'Jockey Puma Deportivo Trucker', 4, 10, 5990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'105744-002.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (24, N'SKU 5891-1425', N'Reloj Puma', N'Reloj Hombre Deportivo Puma', 4, 10, 60990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'855905-999.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (25, N'SKU 4144-1365', N'Juego Deportivo', N'Juego Deportivo Kidscoo', 5, 10, 67990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'861041-999.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (26, N'SKU 3064-4682', N'Carpa Coleman', N'Carpa Para 6 Personas Coleman', 5, 10, 104990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'174621-999.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (27, N'SKU 4541-2213', N'Cuchillo Linterna', N'Cuchillo Linterna Multifuncional Atom', 5, 10, 5990, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'268440-999.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (28, N'SKU 1886-1549', N'Gorro Nike', N'Jockey Nike Negro Baratito', 4, 10, 1, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'jockeyNikeNegro.jpeg', N'', N'', N'', 1)
INSERT [dbo].[Producto] ([id], [codigo], [nombre], [descripcion], [categoria], [stock], [precio], [fechacreacion], [usuariocreacion], [fechabaja], [usuariobaja], [promocion], [preciopromocion], [descuento], [porcentajedescuento], [imagen1], [imagen2], [imagen3], [imagen4], [estado]) VALUES (29, N'SKU 2278-3178', N'Zapatilla Generica', N'Zapatilla Fea Barata Outdoor', 5, 10, 1, CAST(0x0000A7C800000000 AS DateTime), 1, NULL, NULL, 0, 0, 0, 15, N'imagen1.jpg', N'', N'', N'', 1)
SET IDENTITY_INSERT [dbo].[Producto] OFF
/****** Object:  Table [dbo].[Direccion]    Script Date: 07/13/2017 19:48:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Direccion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cliente] [int] NULL,
	[region] [int] NULL,
	[comuna] [int] NULL,
	[calle] [varchar](300) NULL,
	[numero] [int] NULL,
	[block] [varchar](10) NULL,
	[depto] [varchar](10) NULL,
	[detalle] [varchar](50) NULL,
 CONSTRAINT [PK_Direccion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleVenta]    Script Date: 07/13/2017 19:48:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleVenta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[venta] [int] NULL,
	[producto] [int] NULL,
	[precio] [int] NULL,
	[descuento] [int] NULL,
 CONSTRAINT [PK_DetalleVenta] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categoria]    Script Date: 07/13/2017 19:48:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categoria](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
 CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Categoria] ON
INSERT [dbo].[Categoria] ([id], [nombre]) VALUES (1, N'Zapatos')
INSERT [dbo].[Categoria] ([id], [nombre]) VALUES (2, N'Ropa')
INSERT [dbo].[Categoria] ([id], [nombre]) VALUES (3, N'Deportes')
INSERT [dbo].[Categoria] ([id], [nombre]) VALUES (4, N'Accesorios')
INSERT [dbo].[Categoria] ([id], [nombre]) VALUES (5, N'Outdoor')
SET IDENTITY_INSERT [dbo].[Categoria] OFF
