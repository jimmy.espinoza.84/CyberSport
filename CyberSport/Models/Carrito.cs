﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyberSport.Models
{
    public class Carrito
    {
            public int IdProducto { get; set; }
            public string NombreProducto { get; set; }
            public string ImagenProducto { get; set; }
            public int Cantidad { get; set; }
            public int PrecioUnitario { get; set; }
    }
}