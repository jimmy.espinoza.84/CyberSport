//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CyberSport.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Venta
    {
        public int id { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public Nullable<int> cliente { get; set; }
        public Nullable<int> total { get; set; }
        public Nullable<int> direccion { get; set; }
        public string nombrerecibe { get; set; }
        public Nullable<int> estado { get; set; }
    }
}
