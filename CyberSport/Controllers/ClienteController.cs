﻿using CyberSport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CyberSport.Controllers
{
    public class ClienteController : Controller
    {
        private CyberSportEntities db = new CyberSportEntities();
        //
        // GET: /Cliente/
        public ActionResult Index()
        {
            return View(new Usuario());
        }

        [HttpPost]
        public ActionResult Index(Usuario cliente)
        {
            if (db.Usuario.Where(r => r.email == cliente.email).Count() > 0)
            {
                return View(cliente);
            }
            else
            {
                cliente.estado = 1;
                db.Usuario.Add(cliente);
                db.SaveChanges();
                Session["User"] = cliente;
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Login(string email, string pwd)
        {

            var q = from p in db.Usuario
                    where p.email == email
                    && p.password == pwd
                    select p;
            
            if (q.Any())
            {
                Usuario user = q.First();
                Session["User"] = user;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", "Cliente");
            }
        }

        public ActionResult Logout()
        {
            Session["User"] = null;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logueado()
        {

            if (Session["User"] == null)
            {
                ViewBag.nombre = "Bienvenido";
                ViewBag.status = 0;
            }
            else
            {
                CyberSport.Models.Usuario user = Session["User"] as CyberSport.Models.Usuario;
                ViewBag.nombre = user.nombre;
                ViewBag.status = 1;
            }

            return PartialView(ViewBag);
        }

    }
}