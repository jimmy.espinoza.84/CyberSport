﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CyberSport.Models;

namespace CyberSport.Controllers
{
    public class HomeController : Controller
    {
        private CyberSportEntities db = new CyberSportEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Catalogo(int id=0)
        {
            List<Producto> pro = new List<Producto>();
            if (id == 0)
            {
                pro = db.Producto.Where(e => e.estado == 1).ToList();
            }
            else
            {
                pro = db.Producto.Where(e => e.estado == 1).Where(c => c.categoria == id).ToList();
            }
            return View(pro);
        }

        public ActionResult Categorias()
        {
            List<Categoria> cat = db.Categoria.ToList();
            return PartialView(cat);
        }
      
    }
}