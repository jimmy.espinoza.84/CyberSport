﻿using CyberSport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CyberSport.Controllers
{
    public class CarroController : Controller
    {

        private CyberSportEntities db = new CyberSportEntities();

        //
        // GET: /Carro/
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ListarCarrito()
        {
            List<Carrito> car = new List<Carrito>();

            if (Session["Carro"] != null)
            {
                car = Session["Carro"] as List<Carrito>;
            }

            return PartialView(car);
        }

        public ActionResult AgregaCarrito(int id)
        {
            Carrito c = new Carrito();
            List<Carrito> car = new List<Carrito>();
            Producto pro = db.Producto.FirstOrDefault(i => i.id == id);

            if (Session["Carro"] != null)
            {
                car = Session["Carro"] as List<Carrito>;
            }

            if (pro != null)
            {
                c = car.FirstOrDefault(p => p.IdProducto == id);
                if (c == null)
                {
                    car.Add(new Carrito { IdProducto=id, NombreProducto=pro.nombre, ImagenProducto=pro.imagen1, Cantidad=1, PrecioUnitario=int.Parse(pro.precio.ToString()) });
                }
                else
                {
                    car.FirstOrDefault(p => p.IdProducto == id).Cantidad++;
                }
                
            }

            Session["Carro"] = car;

            return RedirectToAction("ListarCarrito", "Carro");

        }

        public ActionResult BotonPagar()
        {
            
            if (Session["User"] == null)
            {
                ViewBag.boton = false;
            }
            else
            {
                ViewBag.boton = true;
            }

            return PartialView(ViewBag);

        }

        public ActionResult Pagar()
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else 
            {
                List<Carrito> car = new List<Carrito>();
                car = Session["Carro"] as List<Carrito>;
                return View(car);
            }
        }

    }
}